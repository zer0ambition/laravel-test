<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('tel', 255);
            $table->string('login', 255)->unique();
            $table->string('comp_name', 255);
            $table->string('comp_inn', 255);
            $table->string('comp_legal_addr', 255);
            $table->string('comp_post_addr', 255);
            $table->string('comp_checking_acc', 255);
            $table->string('comp_bik', 255);
            $table->string('comp_tax_system', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('tel');
            $table->dropColumn('login');
            $table->dropColumn('comp_name');
            $table->dropColumn('comp_inn');
            $table->dropColumn('comp_legal_addr');
            $table->dropColumn('comp_post_addr');
            $table->dropColumn('comp_checking_acc');
            $table->dropColumn('comp_bik');
            $table->dropColumn('comp_tax_system');
        });
    }
}
