<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('type_id');
            $table->string('comment', 255);
            $table->string('props', 255)->default(json_encode(new stdClass));
            $table->dateTime('last_auth', 0)->nullable();
            $table->boolean('blocked')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type_id');
            $table->dropColumn('comment');
            $table->dropColumn('props');
            $table->dropColumn('last_auth');
            $table->dropColumn('blocked');
        });
    }
}
