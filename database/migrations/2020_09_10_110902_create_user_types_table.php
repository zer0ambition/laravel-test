<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('name');
            $table->boolean('can_be_deleted');
            $table->boolean('can_modify_options');
            $table->boolean('can_create_users');
            $table->boolean('can_operate_offers');
            $table->boolean('can_operate_tenders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }
}
