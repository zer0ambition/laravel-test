<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserType;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $userType = $this->userType->find($this->type_id);

        return [
            'id' => $this->id,
            'email' => $this->email,
            'tel' => $this->tel,
            'compName' => $this->comp_name,
            'compInn' => $this->comp_inn,
            'compLegalAddr' => $this->comp_legal_addr,
            'compPostAddr' => $this->comp_post_addr,
            'compCheckingAddr' => $this->comp_checking_acc,
            'compBik' => $this->comp_bik,
            'compTaxSystem' => $this->comp_tax_system,
            'props' => $this->props,
            'created' => $this->created_at->timestamp,
            'blocked' => $this->blocked,
            'type' => self::prepareUserType($userType),
        ];
    }

    /**
     * Format UserType
     *
     * @param UserType $type Existing UserType
     *
     * @return array
     */
    public static function prepareUserType(UserType $type)
    {
        return [
            'id' => $type->id,
            'name' => $type->name
        ];
    }
}
