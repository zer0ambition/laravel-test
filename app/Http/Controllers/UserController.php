<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use App\Models\SearchModels\UserSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Response\JSONResponse;
use App\Http\Response\ErrorResponse;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceCollection;
use App\Helpers\ToSnakeCaseHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the user resource collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::query();
        $query = UserSearch::search($query, $request);
        $users = $query->get();

        return new JSONResponse(['data' => new UserResourceCollection($users), 'message' => trans('http.success')]);
    }

    /**
     * Show the form for creating a new user resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = User::rules_create();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return new JSONResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422);
        }

        DB::beginTransaction();
        try {
            $user = User::create(array_merge(
                ToSnakeCaseHelper::requestAttrToSnakeCase($request),
                ['password' => bcrypt($request->password)]
            ));

        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JSONResponse(['data' => new UserResource($user), 'message' => trans('http.create')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the user resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (!$user->exists) {
            return new ErrorResponse(trans('http.resource_not_found'));
        }

        return new JSONResponse(['data' => new UserResource($user), 'message' => trans('http.success')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified user model.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = User::rules_update();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return new JSONResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422);
        }

        DB::beginTransaction();
        try {
            if ($request->password) {
                $user->fill(array_merge(
                    ToSnakeCaseHelper::requestAttrToSnakeCase($request),
                    ['password' => bcrypt($request->password)]
                ));
            } else {
                $user->fill(ToSnakeCaseHelper::requestAttrToSnakeCase($request));
            }
            $user->update();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JSONResponse(['data' => new UserResource($user), 'message' => trans('http.patch')]);
    }

    /**
     * Update current user model.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selfUpdate(Request $request)
    {
        $user = auth()->user();

        $rules = User::rules_update();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return new JSONResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422);
        }

        DB::beginTransaction();
        try {
            if ($request->password) {
                $user->fill(array_merge(
                    ToSnakeCaseHelper::requestAttrToSnakeCase($request),
                    ['password' => bcrypt($request->password)]
                ));
            } else {
                $user->fill(ToSnakeCaseHelper::requestAttrToSnakeCase($request));
            }
            $user->update();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JSONResponse(['data' => new UserResource($user), 'message' => trans('http.patch')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
