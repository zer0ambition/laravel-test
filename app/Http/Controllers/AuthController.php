<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Response\JSONResponse;
use App\Http\Response\TokenResponse;
use App\Http\Resources\UserResource;
use Validator;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->only('email', 'password'), [
            'email' => 'required|email',
            'password' => 'required|string|min:4',
        ]);

        if ($validator->fails()) {
            return new JSONResponse(['errors' => $validator->errors(), 'message' => trans('auth.failed')], 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return new JSONResponse(['errors' => $validator->errors(), 'message' => trans('auth.failed')], 401);
        }

        return new TokenResponse($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {

        $rules = User::rules_create();
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return new JSONResponse(['errors' => $validator->errors(), 'message' => trans('auth.failed')], 422);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        auth()->login($user);

        return new JSONResponse(['message' => trans('auth.success_registration')], 200);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        try {
            auth()->logout();
        } catch (Exception $e) {
            return new MsgResponse($e->getMessage());
        }

        return new MsgResponse(trans('auth.logout'));
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile() {
        if (auth()->user()) {
            return new JSONResponse(['data' => new UserResource(auth()->user()), 'message' => 'Success']);
        }
        return new JSONResponse(['message' => trans('auth.unauth')], 401);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
