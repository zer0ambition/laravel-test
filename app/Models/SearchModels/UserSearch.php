<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class UserSearch
{
    public const DEFAULT_ORDER_BY = 'id';
    public const DEFAULT_SORT_DIRECTION = 'asc';
    public const DEFAULT_LIMIT = 15;

    public static function search(Builder $query, Request $request)
    {
        if ($request->typeId) {
            $query->orWhere('type_id', '=', "%{$request->typeId}%");
        }

        $orderBy = $request->sort ?? self::DEFAULT_ORDER_BY;
        $orderDirection = $request->order ?? self::DEFAULT_SORT_DIRECTION;
        $limit = $request->limit ?? self::DEFAULT_LIMIT;
        $offset = $request->offset ?? 0;

        $query->orderBy($orderBy, $orderDirection)
            ->offset($offset)
            ->limit($limit);

        return $query;
    }
}
