<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Query\Builder;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * SuperAdmin user type_id
     *
     * @var integer
     */
    public const SUPERADMIN_TYPE = 1;
    /**
     * Admin user type_id
     *
     * @var integer
     */
    public const ADMIN_TYPE = 2;

    /**
     * Manager user type_id
     *
     * @var integer
     */
    public const MANAGER_TYPE = 3;

    /**
     * Provider user type_id
     *
     * @var integer
     */
    public const PROVIDER_TYPE = 4;

    /**
     * Fresh user type_id
     *
     * @var integer
     */
    public const FRESH_TYPE = 5;

    /**
     * Validator creating rules array
     *
     * @var array
     */
    public static function rules_create()
    {
      return [
            'email' => 'sometimes|required|email|unique:users',
            'login' => 'sometimes|required|unique:users',
            'password' => 'required|string|confirmed|min:4',
            'tel' => 'required|string',
            'compName' => 'string',
            'type_id' => 'integer',
        ];
    }

    /**
     * Validator creating rules array
     *
     * @var array
     */
    public static function rules_update()
    {
      return [
            'email' => 'sometimes|email|unique:users',
            'login' => 'sometimes|unique:users',
            'password' => 'string|confirmed|min:4',
            'tel' => 'required|string',
            'compName' => 'string',
            'type_id' => 'integer',
            'props' => 'string',
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'login',
        'password',
        'tel',
        'props',
        'comp_name',
        'comp_inn',
        'comp_legal_addr',
        'comp_post_addr',
        'comp_checking_acc',
        'comp_bik',
        'comp_tax_system',
        'type_id',
        'last_auth',
        'comment'
    ];

    /**
     * Default value of attributes
     *
     * @var array
     */
    protected $attributes = [
        'type_id' => self::FRESH_TYPE,
        'tel' => '',
        'comp_name' => '',
        'comp_inn' => '',
        'comp_legal_addr' => '',
        'comp_post_addr' => '',
        'comp_checking_acc' => '',
        'comp_bik' => '',
        'comp_tax_system' => '',
        'blocked' => false,
        'last_auth' => null,
        'props' => '{}',
        'name' => 'name',
        'comment' => ''
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'refresh_token',
        'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'blocked' => 'boolean',
    ];

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();  // Eloquent model method
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
        ];
    }

    /**
     * Relation to usertype table
     *
     * @return HasOne
     */
    public function userType()
    {
        return $this->hasOne(UserType::class, 'id', 'type_id');
    }
}
