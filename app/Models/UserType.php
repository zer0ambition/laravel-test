<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_types';

    protected $fillable = [
        'name',
        'can_be_deleted',
        'can_modify_options',
        'can_create_users',
        'can_modify_options',
        'can_operate_offers',
        'can_operate_tenders',
    ];

    /**
     * Returns name attribute
     *
     * @param mixed $name Attribute
     *
     * @return string
     */
    public function getNameAttribute($name)
    {
        return trans($name);
    }

    /**
     * Relation to users table
     *
     * @return BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'id', 'type_id');
    }
}
