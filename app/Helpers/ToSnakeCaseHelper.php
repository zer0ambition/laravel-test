<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ToSnakeCaseHelper
{
    /**
     * Mutate request keys to snake case.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public static function requestAttrToSnakeCase(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            $newArr[Str::snake($key)] = $value;
        }
        return $newArr;
    }
}
