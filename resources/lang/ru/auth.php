<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Данные учётные данные не были найдены у нас.',
    'throttle' => 'Слишком много запросов. Остынь :seconds секунд.',
    'no_login' => 'Нет логина',
    'logout' => 'Вышел из системы',
    'success_login' => 'Успешно авторизировался',
    'unauth' => 'Неавторизирован',
    'noauth' => 'Неавторизирован',
    'success_registration' => 'Успешная регистрация'
];
