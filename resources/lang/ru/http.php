<?php

return [
    'delete' => 'Успешно был удалён :item.',
    'patch' => 'Успешно был обновлён :item.',
    'create' => 'Успешно был создан :item.',
    'resource_not_found' => 'Ресурс :resource не найден',
    'route_not_found' => 'Путь не найден.',
    'wrong_data' => 'Неверные данные в запросе.',
    'too_many' => 'Слишком много запросов.',
    'forbidden' => 'Нет доступа.',
    'file_not_valid' => 'Файл :name неверный',
    'avatar_not_valid' => 'Аватар :name неверного формата. Возможные типы данных аватара :mimes',
    'success' => 'Успешно',
    'error' => 'Ошибка!',
    'file_upload' => 'Файл :name не был загружен на наши сервера потому что :error',
    'feedback_sent' => 'Спасибо за обратную связь! Мы свяжемся с вами в скором времени.',
    'post_too_large' => 'Загрузка файла размером :size превышает лимит загрузки :limit.'
];
