<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//auth
Route::group([
    'middleware' => 'api',
], function ($router) {
    Route::post('signin', 'AuthController@login')->name('signin');
    Route::post('signup', 'AuthController@register');
    Route::post('signout', 'AuthController@logout');
    //Route::post('refresh', 'AuthController@refresh');
});

//user
Route::prefix('users')->middleware('auth:api')->group(function () {
    Route::prefix('@me')->group(function () {
        Route::get('/', 'AuthController@profile');
        Route::patch('/', 'UserController@selfUpdate');
    });
    Route::get('/', 'UserController@index');

    Route::prefix('{user}')->group(function () {
        Route::get('/', 'UserController@show');
        Route::patch('/', 'UserController@update');
        Route::delete('/{user}', 'UserController@delete');
    });

    Route::post('/', 'UserController@create');
});
